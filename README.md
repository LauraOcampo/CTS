# ¿Qué es *CTS*?
SENA
 CTS, sigla en inglés Class Time in Software (Tiempo de Clase en Software), es
una plataforma virtual de horarios de la sede del SENA Barrio Colombia. Este
software controlará la disponibilidad de ambientes de formación y permitirá con
facilidad la consulta de cada uno de los horarios de su respectivo programa de
formación, la ubicación y la hora a los instructores y aprendices de la sede. 

***


## Objetivo general

Diseñar y desarrollar un Software para el control y programación de horarios del SENA de la sede barrio colombia.

***


## Objetivos Especificos

- El software brindará una interfaz gráfica de muy fácil manejo para todos los usuarios.
- Diagnosticar la situación actual de la programación que se llevan a cabo en el Sena.
- Mostrar la información de los horarios al usuario. 
- Administrar el manejo del horario y detallar la funcionalidad que se está utilizando. 
- Analizar e interpretar la información recolectada.

***


## Planteamiento del Problema

En el Sena existen fallas en la creación, organización y consulta de horarios.
Por lo tanto se generan pérdidas de tiempo.


***

## Alcance

El proyecto •CTS• (Class Time in software), será la mejor alternativa para todos los usuarios 
(instructores, aprendices y administrador) para crear y controlar la disponibilidad de los ambientes de formación
y consultar los horarios de cada programa de formación de la sede barrio colombia. 

***


## Justificación

La creación de este software permita llevar el control y organización de los horarios en el Sena, logrará que los usuarios 
y el administrador estén satisfechos por el orden y la rapidez al momento de consultar el horario y por su fácil manejo.